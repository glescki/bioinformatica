# for unpacking data
import pickle
import numpy as np
import os.path

# constant values
ITEMS_PER_SET = 10000
IMAGE_SIZE = 3072
IMAGE_WIDTH = 4
IMAGE_HEIGHT = 101


def encode_data(sequence):
    data_vector = np.zeros([len(sequence), 4])
    i = 0
    for chr in sequence:
        if chr == "A": ind = 0
        if chr == "T": ind = 1
        if chr == "C": ind = 2
        if chr == "G": ind = 3

        data_vector[i][ind] = 1
        i = i + 1

    return data_vector


def encode_label(label):
    labels_vector = np.zeros([2])

    # to implement
    labels_vector[label] = 1

    return labels_vector


class Data:
    def __init__(self):
        self.train_data = {'labels': [], 'sequences': []}
        self.test_data = {'labels': [], 'sequences': []}
        self.train_ind = 0
        self.test_ind = 0

    def prep_data(self, filename):
        PATH = os.path.abspath(os.path.dirname(__file__))
        BASE_PATH = os.path.join(PATH, 'deepbind/' + filename)

        train_file = os.path.join(BASE_PATH, 'train.fa')
        test_file = os.path.join(BASE_PATH, 'test.fa')
        i = 0
        with open(train_file, 'rb') as fo:
            for line in fo:
                data_dict = str(line, 'utf-8').rstrip()
                # Every even line in the files has the label (1 or 0)
                if i % 2 == 0:
                    self.train_data['labels'].append(int(data_dict[2]))
                # And every odd line has the respective sequence
                else:
                    one_hot_matrix = encode_data(data_dict)
                    self.train_data['sequences'].append(one_hot_matrix)
                i = i + 1
        i = 0
        with open(test_file, 'rb') as fo:
            for line in fo:
                data_dict = str(line, 'utf-8').rstrip()
                # Every even line in the files has the label (1 or 0)
                if i % 2 == 0:
                    self.test_data['labels'].append(int(data_dict[1]))
                # And every odd line has the respective sequence
                else:
                    one_hot_matrix = encode_data(data_dict)
                    self.test_data['sequences'].append(one_hot_matrix)
                i = i + 1

    def next_train_batch(self, batch_size):
        current_sequences = np.asarray(self.train_data['sequences'])
        current_labels = self.train_data['labels']

        indices = np.random.choice(current_sequences.shape[0], batch_size)

        sequences = current_sequences[indices, ...]

        labels = [encode_label(current_labels[i]) for i in indices]
        labels = np.reshape(labels, (batch_size, 2))

        self.train_ind = self.train_ind + batch_size

        return sequences, labels

    def next_test_batch(self, batch_n, batch_size):
        current_sequences = np.asarray(self.test_data['sequences'])
        current_labels = self.test_data['labels']

        sequences = np.empty([batch_size, 101, 4])
        labels = np.empty([batch_size, 2])

        # indices = range(self.test_ind, self.test_ind + batch_size)
        current_start = batch_size * batch_n
        for i, j in enumerate(range(current_start, min(current_start + batch_size, current_sequences.shape[0]))):
            sequences[i] = current_sequences[j]
            labels[i] = encode_label(current_labels[j])
        labels = np.reshape(labels, (batch_size, 2))

        return sequences, labels

    def get_test_data(self):
        return np.asarray(self.test_data['sequences'])

    def get_test_labels(self):
        labels = np.empty([1000, 2])
        for i in range(1000):
            labels[i] = encode_label(self.test_data['labels'][i])
        return labels
# data = Data()
# data.prep_data('BHLHE40_K562_BHLHE40_-NB100-1800-_Stanford')
# data.next_test_batch(0, 1)