# Tensorflow imports
import tensorflow as tf

# Define model parameters here
from HelperFunctions import train_helper
from HelperFunctions import models_helper


def forward(images_batch, keep_prob):
    with tf.name_scope('MedCNN'):
        # 1st Layer: Conv (w ReLu) -> Pool -> Dropout
        conv1 = models_helper.conv1d(images_batch, 4, 64, 9, 1, 'SAME', True, 'conv1')
        conv1 = tf.expand_dims(conv1, 1)
        pool1 = tf.nn.max_pool(conv1, ksize=[1, 2, 2, 1], strides=[1, 1, 1, 1], padding='SAME')
        pool1 = tf.squeeze(pool1, [1])
        drop_out1 = tf.nn.dropout(pool1, keep_prob)

        # 2nd Layer: Conv (w ReLu) -> Pool -> Dropout
        conv2 = models_helper.conv1d(drop_out1, 64, 64, 5, 1, 'SAME', True, 'conv2')
        conv2 = tf.expand_dims(conv2, 1)
        pool2 = tf.nn.max_pool(conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        pool2 = tf.squeeze(pool2, [1])
        drop_out2 = tf.nn.dropout(pool2, keep_prob)

        # 3rd Layer: Conv (w ReLu) -> Pool -> Dropout
        conv3 = models_helper.conv1d(drop_out2, 64, 64, 3, 1, 'SAME', True, 'conv3')
        conv3 = tf.expand_dims(conv3, 1)
        pool3 = tf.nn.max_pool(conv3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        pool3 = tf.squeeze(pool3, [1])
        drop_out3 = tf.nn.dropout(pool3, keep_prob)

        # 4th Layer: Conv (w ReLu) -> Pool -> Dropout -> Max
        conv4 = models_helper.conv1d(drop_out3, 64, 64, 3, 1, 'SAME', True, 'conv4')
        conv4 = tf.expand_dims(conv4, 1)
        pool4 = tf.nn.max_pool(conv4, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        pool4 = tf.squeeze(pool4, [1])
        drop_out4 = tf.nn.dropout(pool4, keep_prob)
        drop_out4 = tf.reduce_max(drop_out4, axis=1)

        # 5th Layer: FC (w/o ReLu)
        fc1 = models_helper.fc(drop_out4, 64, 2, 'fc1', relu=False)

    return fc1
