# Tensorflow imports
import tensorflow as tf

# Define model parameters here
from HelperFunctions import train_helper
from HelperFunctions import models_helper


def forward(images_batch, keep_prob):
    with tf.name_scope('MedCNN'):
        # 1st Layer: Conv (w ReLu) -> Pool -> Dropout
        conv1 = models_helper.conv1d(images_batch, 4, 64, 9, 1, 'SAME', True, 'conv1')
        conv1 = tf.expand_dims(conv1, 1)
        pool1 = tf.nn.max_pool(conv1, ksize=[1, 2, 2, 1], strides=[1, 1, 1, 1], padding='SAME')
        pool1 = tf.squeeze(pool1, [1])
        drop_out1 = tf.nn.dropout(pool1, keep_prob)

        # 2nd Layer: Conv (w ReLu) -> Pool -> Dropout -> Max
        conv2 = models_helper.conv1d(drop_out1, 64, 64, 5, 1, 'SAME', True, 'conv2')
        conv2 = tf.expand_dims(conv2, 1)
        pool2 = tf.nn.max_pool(conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        pool2 = tf.squeeze(pool2, [1])
        drop_out2 = tf.nn.dropout(pool2, keep_prob)
        drop_out2 = tf.reduce_max(drop_out2, axis=1)

        # 3rd Layer: FC (w/o ReLu)
        flattened = tf.reshape(drop_out2, [-1, 64])
        fc1 = models_helper.fc(flattened, 64, 2, 'fc1', relu=False)

    return fc1
