# AlexNet architectural graph

# Tensorflow imports
import tensorflow as tf

# Define model parameters here
from HelperFunctions import train_helper
from HelperFunctions import models_helper


def forward(images_batch, training):
    with tf.name_scope('alexnet'):
        # 1st Layer: Conv (w ReLu) -> Pool
        conv1 = models_helper.conv1d(images_batch, 4, 64, 5, 1, 'SAME', True, 'conv1')
        conv1 = tf.expand_dims(conv1, 1)
        pool1 = tf.nn.max_pool(conv1, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME')
        pool1 = tf.squeeze(pool1, [1])

        # 2nd Layer: Conv (w ReLu) -> Pool
        conv2 = models_helper.conv1d(pool1, 64, 64, 5, 1, 'SAME', True, 'conv2')
        conv2 = tf.expand_dims(conv2, 1)
        pool2 = tf.nn.max_pool(conv2, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME')
        pool2 = tf.squeeze(pool2, [1])

        # 3rd Layer: FC (w ReLu)
        flattened = tf.reshape(pool2, [-1, 26 * 64])
        fc1 = models_helper.fc(flattened, 26 * 64, 384, 'fc1')

        # 4th Layer: FC (w ReLu)
        fc2 = models_helper.fc(fc1, 384, 192, 'fc2')

        # 5th Layer: FC (w/o ReLu) -> Softmax
        fc3 = models_helper.fc(fc2, 192, 10, 'fc3')

        # 6th Layer: FC (w/o ReLu) -> Softmax
        fc4 = models_helper.fc(fc3, 10, 2, 'fc4', relu=False)

    return fc4
