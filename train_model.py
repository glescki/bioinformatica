from __future__ import print_function
import os.path
import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
from six.moves import cPickle as pickle
from datetime import datetime
from time import sleep
from Models import LargeCNN as model
from Data import input_data
from HelperFunctions import train_helper
from HelperFunctions import models_helper

learning_rate = 0.01
epochs = 15
batch_size = 256

# mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

# Input tensor
x = tf.placeholder(tf.float32, [None, 101, 4])
# Label tensor
y = tf.placeholder(tf.float32, [None, 2])
# Dropout tensor
keep_prob = tf.placeholder(tf.float32)
# Construct model
logits = model.forward(x, keep_prob)
y_ = tf.nn.softmax(logits)

# Calculate Loss
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=y))
# Training
optimiser = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cross_entropy)
# Accuracy
correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))


init_op = tf.global_variables_initializer()

data = input_data.Data()
data.prep_data('BHLHE40_K562_BHLHE40_-NB100-1800-_Stanford')

with tf.Session() as sess:
    # initialise the variables
    sess.run(init_op)
    total_batch = int(43994 / batch_size)
    print('{} Starting training LargeCNN w Drop out'.format(datetime.now()))
    for epoch in range(epochs):
        print('EPOCH : ', epoch, ' BEGIN')
        avg_cost = 0
        for step in range(total_batch):
            batch_x, batch_y = data.next_train_batch(batch_size)
            _, c = sess.run([optimiser, cross_entropy],
                            feed_dict={x: batch_x, y: batch_y, keep_prob: 0.5})
            avg_cost += c / total_batch

            if step % 25 == 0:
                # Print status on stdout
                print('{} Loss on step {}: {}'.format(datetime.now(), step, c))

        print('EPOCH : ', epoch, ' END')
        test_sequences = data.get_test_data()
        test_labels = data.get_test_labels()
        test_acc = sess.run(accuracy,
                            feed_dict={x: test_sequences, y: test_labels, keep_prob: 1.0})
        print("{} Epoch:".format(datetime.now()), (epoch + 1),
              "cost =", "{:.3f}".format(avg_cost),
              "test accuracy: {:.6f}".format(test_acc))
        # for i in range(0, test_images.shape[0]):
        #     res = sess.run(y_, {x: [test_images[i]]})
        #     print("Result: " + str(res) + " Actually: " + str(test_labels[i]))
        print('\n-----------------------------------------------\n')

    print("\nTraining complete!")
    # print(sess.run(accuracy, feed_dict={x: data.get_test_data(), y: data.get_test_labels()}))
