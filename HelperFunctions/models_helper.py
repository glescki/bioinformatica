import tensorflow as tf


def batch_norm(x, training, scope):
    """
    :param x: Input to the layer
    :param training: If phase is training or test
    :param scope: Name of the scope
    :return: x normalized
    """
    tf.contrib.layers.batch_norm(x, center=True, scale=True, is_training=training, scope=scope)

    return x


def conv2d(x, channels_in, channels_out, kernel_size, stride_size, padding_mode, use_bias, scope):
    """
    :param x: Input to the layer
    :param channels_in: Amount of channels in the input
    :param channels_out: Amount of channels in the output
    :param kernel_size: Size of the weights kernels
    :param stride_size: Stride size
    :param padding_mode: Type of padding
    :param use_bias: True or False the add of an bias
    :param scope: Name of the scope
    :return: y, input convoluted with weights_conv and added with biases
    """
    with tf.variable_scope(scope):

        weights_conv = tf.get_variable('weights_conv', shape=[kernel_size, kernel_size, channels_in, channels_out],
                                       initializer=tf.contrib.layers.xavier_initializer(), trainable=True)

        conv_layer = tf.nn.conv2d(x, weights_conv, strides=[1, stride_size, stride_size, 1], padding=padding_mode)

        if use_bias:
            bias_conv = tf.get_variable('bias_conv', shape=[channels_out],
                                        initializer=tf.contrib.layers.xavier_initializer(), trainable=True)
        else:
            bias_conv = tf.Variable(tf.zeros([channels_out]))

        pre_activation = tf.nn.bias_add(conv_layer, bias_conv)

        y = tf.nn.relu(pre_activation)
    return y


def conv1d(x, channels_in, channels_out, kernel_size, stride_size, padding_mode, use_bias, scope):
    """
    :param x: Input to the layer
    :param channels_in: Amount of channels in the input
    :param channels_out: Amount of channels in the output
    :param kernel_size: Size of the weights kernels
    :param stride_size: Stride size
    :param padding_mode: Type of padding
    :param use_bias: True or False the add of an bias
    :param scope: Name of the scope
    :return: y, input convoluted with weights_conv and added with biases
    """
    with tf.variable_scope(scope):

        weights_conv = tf.get_variable('weights_conv', shape=[kernel_size, channels_in, channels_out],
                                       initializer=tf.contrib.layers.xavier_initializer(), trainable=True)
        conv_layer = tf.nn.conv1d(x, weights_conv, stride=stride_size, padding=padding_mode)

        if use_bias:
            bias_conv = tf.get_variable('bias_conv', shape=[channels_out],
                                        initializer=tf.contrib.layers.xavier_initializer(), trainable=True)
        else:
            bias_conv = tf.Variable(tf.zeros([channels_out]))

        pre_activation = tf.nn.bias_add(conv_layer, bias_conv)

        y = tf.nn.relu(pre_activation)
    return y


def fc(x, num_in, num_out, scope, relu=True):
    """
    :param x: Input to the layer
    :param num_in: Input size
    :param num_out: Output size
    :param scope: Name of the scope
    :param relu: Boolean to check if relu after the fc, default value is True
    :return: y if relu=true, else fc
    """

    with tf.variable_scope(scope):

        weights = tf.get_variable('weights', shape=[num_in, num_out], trainable=True)
        biases = tf.get_variable('biases', shape=[num_out], trainable=True)

        fc = tf.nn.bias_add(tf.matmul(x, weights), biases)

        if relu:
            y = tf.nn.relu(fc)
            return y
        else:
            return fc


def loss(predictions, gt_labels):
    """
    :param predictions: Output of neural network
    :param gt_labels: Respective labels
    :return: Cross entropy loss
    """
    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=gt_labels, logits=predictions))

    return cross_entropy


def training(loss, learning_rate, optimizer_num):
    """
    Sets up the training Ops.

    Args:
    loss: Loss tensor, from loss().
    learning_rate: The learning rate to use for gradient descent.

    Returns:
    train_op: The Op for training.
    """
    # to implement
    # SGD
    if optimizer_num == 1:
        optimizer = tf.train.GradientDescentOptimizer(learning_rate)

    # Adam
    if optimizer_num == 2:
        optimizer = tf.train.AdamOptimizer(learning_rate)

    # RMSProp
    if optimizer_num == 3:
        optimizer = tf.train.RMSPropOptimizer(learning_rate)

    train_op = optimizer.minimize(loss)

    return train_op
