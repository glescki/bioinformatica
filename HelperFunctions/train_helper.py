# Define overall training function, like e.g. data reading or validation during training.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np

from Data import input_data


def placeholder_inputs(batch_size, height_size, width_size, channels_size, output_size):
    # to implement
    images_placeholder = tf.placeholder(tf.float32, shape=(batch_size, height_size,
                                                           width_size, channels_size))
    labels_placeholder = tf.placeholder(tf.float32, shape=(batch_size, output_size))

    return images_placeholder, labels_placeholder


def do_eval(sess, accuracy, images_placeholder, labels_placeholder, data,
            is_valid, epoch, steps, batch_size):
    # to implement
    # And run one epoch of eval.
    true_count = 0  # Counts the number of correct predictions.
    steps_per_epoch = steps // batch_size
    num_examples = steps
    for step in range(steps_per_epoch):
        if is_valid:
            x, y = data.next_valid_batch(step, batch_size)
        else:
            x, y = data.next_test_batch(step, batch_size)

        true_count += sess.run(accuracy, feed_dict={images_placeholder: x, labels_placeholder: y})
    precision = float(true_count) / num_examples
    print('Num examples: %d  Num correct: %d  Precision @ 1: %0.04f' %
          (num_examples, true_count, precision))
        
    return precision
